# Memoview.py, part of the turing machine project
# Created by Guillaume Ducrocq

# This function show part of the memory (an array of 0 or 1)
# It point to the current memory block and show the ones around

import sys

import glob

def show_mem(memory: list):
    tablen = len(memory) - 1
    if (glob.mem_index > tablen):
        sys.exit("FATAL : Index in uninitialized area")
    if (glob.mem_index < 0):
        sys.exit("FATAL : Index is under 0")
    maxlen = glob.mem_index + 2
    index = glob.mem_index - 2
    print ("-", end="")
    while (index <= maxlen):
        if (index < 0 or index > tablen):
            print (" -[#]- ", end="")
        else:
            print (" -[" + str(memory[index]) + "]- ", end="")
        print ("-", end="")
        index = index + 1
    print ("\n                    ^")
